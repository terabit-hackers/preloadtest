#!/bin/bash

rm ./testprog ./preloadfail.so > /dev/null 2>&1

gcc -o testprog ./testprog.c &&
gcc -Wall -fPIC -shared -o preloadfail.so preloadfail.c &&
LD_PRELOAD=./preloadfail.so ./testprog

if [ $? -ge 1 ]
then
  echo 'Error!! LD_PRELOAD works on this system,please disable this functionality'
  exit 1 
fi

rm ./testprog ./preloadfail.so  > /dev/null 2>&1

exit  1

