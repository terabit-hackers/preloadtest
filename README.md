# README #

This is nothing fancy, just clone or download this and run:

./build_and_test.sh

It just checks if the usage of LD_PRELOAD works on the system. 

I use this for checking system hardening (in addition to many other tests). 

I'll put up a patch and instructions on how to disable this on gentoo soon. 

Contact me at ##hackers on irc.freenode.net for more info.